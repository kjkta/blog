import fs from "fs";
import path from "path";
import marked from "marked";
import Prism from "prismjs";
import { loadFront } from "yaml-front-matter";
// Import react for parsing syntax
// eslint-disable-next-line
import React from "react";

const postsDir = "./posts";

const postFiles = fs.readdirSync(postsDir).filter(function(stat) {
  return !fs.lstatSync(path.join(postsDir, stat)).isDirectory();
});

marked.setOptions({
  highlight: function(code, language) {
    if (!Prism.languages[language]) {
      // Try to load non-default prism languages
      try {
        require("prismjs/components/prism-" + language + ".js");
      } catch (e) {
        console.error(
          `Unable to find prism language '${language}' for highlighting.`
        );
      }
    }
    const grammar = Prism.languages[language];
    const highlighted = Prism.highlight(code, grammar, language);
    return highlighted;
  }
});

const posts = postFiles.map(function(file) {
  const fileName = file.split(".")[0];
  const md = fs.readFileSync(path.join(__dirname, "posts", file), "utf-8");
  const { __content, ...frontmatter } = loadFront(md);
  const tokens = marked.lexer(__content);
  const body = marked.parser(tokens);
  return {
    slug: fileName,
    body,
    ...frontmatter
  };
});

export default {
  entry: path.join(__dirname, "src", "index.tsx"),
  Document: ({ Html, Head, Body, children, state }) => {
    const { title, description } = state.config.getSiteData();
    return (
      <Html lang="en-AU">
        <Head>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>{title}</title>
          <meta name="description" content={description} />
        </Head>
        <Body>{children}</Body>
      </Html>
    );
  },
  getSiteData: function() {
    return {
      title: "Kyles' blog",
      description: "A few things I learn along the way...",
      posts
    };
  },
  getRoutes: function() {
    return posts.map(function(post) {
      return {
        path: "/" + post.slug,
        template: "src/post",
        getData: function() {
          return post;
        }
      };
    });
  },
  plugins: [
    "react-static-plugin-typescript",
    [
      require.resolve("react-static-plugin-source-filesystem"),
      {
        location: path.resolve("./src/pages")
      }
    ],
    require.resolve("react-static-plugin-reach-router"),
    require.resolve("react-static-plugin-sitemap")
  ]
};
