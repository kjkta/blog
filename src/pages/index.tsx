import React from "react";
import { useSiteData } from "react-static";
import { Link } from "@reach/router";
import { Post } from "../../types/";

export default function Home() {
  const {
    title,
    description,
    posts
  }: { title: string; description: string; posts: Post[] } = useSiteData();
  return (
    <main
      css={{
        width: "100%",
        maxWidth: 620,
        margin: "0 auto",
        padding: "0 20px"
      }}
    >
      <header
        css={{
          marginTop: 40,
          marginBottom: 60,
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between"
        }}
      >
        <div css={{ flex: 1, "> *": { margin: 5 } }}>
          <h1>{title}</h1>
          <marquee>{description}</marquee>
        </div>
        <img
          alt="Smiling face"
          src={require("../assets/face.jpg")}
          css={{
            borderRadius: "50%",
            width: 100,
            height: 100,
            objectFit: "cover",
            marginRight: 20
          }}
        />
      </header>
      {posts.map(function({ title, slug, spoiler, image }) {
        return (
          <Link key={slug} to={slug}>
            <div
              css={{
                display: "flex",
                alignItems: "center",
                paddingBottom: 40,
                transition: "transform 200ms",
                ":hover, :focus": {
                  transform: "scale(1.015)"
                }
              }}
            >
              <div css={{ flex: 1 }}>
                <h2>{title}</h2>
                <p css={{ color: "#565f67" }}>{spoiler}</p>
              </div>
              {image && (
                <img
                  src={image}
                  alt="Preview image"
                  css={{
                    padding: 10,
                    width: 175,
                    height: 125,
                    objectFit: "cover"
                  }}
                />
              )}
            </div>
          </Link>
        );
      })}
    </main>
  );
}
