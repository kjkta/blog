import React from "react";
import { useRouteData, useSiteData } from "react-static";
import { Link } from "@reach/router";
import { Post } from "../types";

export default function PostPage() {
  const { title }: { title: string } = useSiteData();
  const post: Post = useRouteData();
  return (
    <div>
      <header css={{ padding: "0 20px" }}>
        <Link to="/">
          <h2>{title}</h2>
        </Link>
      </header>
      <div
        css={{
          margin: "40px auto",
          width: "100%",
          maxWidth: 680,
          padding: "0 20px"
        }}
      >
        <h1>{post.title}</h1>
        <p css={{ color: "grey", fontSize: 14 }}>
          Created: {new Date(post.date).toDateString()}
        </p>
        {post.image && (
          <img
            src={post.image}
            alt="Header image"
            css={{ width: "100%", maxHeight: 300, objectFit: "cover" }}
          />
        )}
        <div dangerouslySetInnerHTML={{ __html: post.body }} />
        <div>
          If this helped you, or you can contribute input or have questions,
          please tweet or DM me <a href="https://twitter.com/kjkta_">@kjkta_</a>
          . Thanks for reading!
        </div>
      </div>
    </div>
  );
}
