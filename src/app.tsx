import React from "react";
import { Root, Routes } from "react-static";
import { Router, Location } from "@reach/router";
import ReactGA from "react-ga";

import "index.css";
import "prismjs/themes/prism.css";

function App() {
  return (
    <Root>
      <div
        css={{
          minHeight: "100vh",
          overflow: "auto",
          display: "flex",
          flexDirection: "column"
        }}
      >
        <div css={{ flex: 1 }}>
          <React.Suspense
            fallback={<em>Fetching content (you're in dev mode)</em>}
          >
            <Router>
              <Routes path="*" />
            </Router>
          </React.Suspense>
        </div>
        <footer
          css={{
            fontSize: 15,
            padding: 20,
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "flex-end",
            "> a": {
              marginLeft: 15,
              textDecoration: "underline"
            }
          }}
        >
          <a href="//twitter.com/kjkta_">Twitter</a>
          <a href="//gitlab.com/kjkta/blog">Blog Source</a>
          <a href="//ticketbutler.io">Ticketbutler</a>
          <a href="//tinyrhino.dk">Tiny Rhino</a>
        </footer>
      </div>
      <Location>
        {() => {
          // Analytics
          if (typeof window !== "undefined") {
            ReactGA.initialize("UA-144557504-1");
            ReactGA.pageview(window.location.pathname + window.location.search);
          }
          return null;
        }}
      </Location>
    </Root>
  );
}

export default App;
