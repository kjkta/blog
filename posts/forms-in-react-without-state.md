---
title: Forms in React without state
date: "2019-07-26"
spoiler: Forms CAN be more simple. Input fields should not be contrained by runtime.
image: https://images.unsplash.com/photo-1532264523420-881a47db012d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1567&q=80
---

_Forms CAN be more simple. Input fields should not be contrained by runtime._

Did you forget that the [form API](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form) exists? Sometimes learning cool new tech distracts from to keeping things simple.

We create SO many forms on the web. Some simple, some reactive and some complex. Here we will focus on the simple ones.

Before you create your next form, ask yourself:

**1. Does my UI need to be instantly reactive to input?**

No? You might not need form values in state to react apon.

**2. Are some of my inputs simple HTML inputs? (ie. `<input type="text" />`)**

Yes? Maybe unconrolled inputs are for you (uncontrolled = no `value={...}` & `onChange={...}`)

**3. Could I create at least part of this form in plain HTML?**

Yes? We just need to provide an `onSubmit` function!

Let's try to create a simple form and capture it's input values in an object that could be used to update some state, or POST to an API.

```jsx
function App() {
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
      }}
    >
      <input name="first_name" />
    </form>
  );
}
```

Now, how do we get those values??? Usually they are already ready and waiting in our state. I had to think back to 2005 when we used jQuery `.serialize()` to get each input as key value pairs. But we don't want to import ALL of jQuery and buy into it's ecosystem for just one feature. Luckily there is something else we can use...

```jsx
import serialize from "for-cerial";

function App() {
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        const formValues = serialize(e.target, { hash: true });
        // { first_name: "Kyle Thomson" }
      }}
    >
      <input name="first_name" />
    </form>
  );
}
```

This serializer, when passed `{ hash: true }` option, will generate key/value pairs for each input. Now the values are there for you to fit to your purpose.

Have some previously saved or default values? Just use React's `defaultValue`.

```jsx
import serialize from "for-cerial";

function App({ savedValues }) {
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        const formValues = serialize(e.target, { hash: true });
        // { first_name: "Kyle Thomson" }
      }}
    >
      <input name="first_name" defaultValue={savedValues.first_name} />
    </form>
  );
}
```

But what if we have complex inputs also? Let's try to retain our simplicity, and only add state for the parts that need it.

```jsx
import serialize from "for-cerial";
import DateTimePicker from "your-favouite-picker";

function App() {
  const [date, setDate] = React.useState(Date.now());
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        const formValues = serialize(e.target, { hash: true });
        const values = {
          ...formValues,
          date
        };
        /* { 
            first_name: "Kyle Thomson", 
            date: Fri Jul 26 2019 09:21:12 GMT+0200 (Central European Summer Time) 
        } */
      }}
    >
      <input name="first_name" />
      <DateTimePicker value={date} onChange={setDate} />
    </form>
  );
}
```

We can simply add some state for the complex bits, and merge them in with our simple inputs! Easy!

Hopefully this gives you some inspiration to start bringing parts of your forms back to the simple and reliable days of Web2.0, and can help to dismiss some issues such as input lag in apps with heavy runtimes.
