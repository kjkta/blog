export interface Post {
  slug: string;
  title: string;
  spoiler: string;
  body: string;
  date: string;
  image?: string;
}
